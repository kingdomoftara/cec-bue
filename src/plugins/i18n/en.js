export const en = {
  header: {
    welcome: 'welcome',
    aboutUs: 'about us',
    activities: 'activities',
    contact: 'contact',
    prices: 'prices'
  },
  homepage: {
    blurb:
      "Culture, Éducation et Création.Toute l'actualité et les activités du CEC à Bué. Cours de musique, soutien scolaire et actions culturelles."
  }
}
