export const fr = {
    shared: {
      clickhere: 'cliquez ici'
    },
    header: {
      welcome: 'accueil',
      aboutUs: 'nous',
      activities: 'activités',
      contact: 'contact',
      prices: 'tarifs'
    },
    homepage: {
      blurb:
        "Culture, Éducation et Création.Toute l'actualité et les activités du CEC à Bué. Cours de musique, soutien scolaire et actions culturelles."
    },
    aboutus: {
      description_title: 'CEC, c\'est aussi... Casey Et Cyril !',
      description_text_1: 'Notre projet nous ressemble. Il est sympa et ouvert, il vous veut d\'abord du bien.',
      description_text_2: 'Nous sommes musiciens, de métier et d\'envie, mais ce n\'est pas tout. Nous aimons voyager, cuisiner, faire des rencontres, lire, rire, inviter des amis, partir à l\'aventure, ne rien faire, découvrir, faire des projets fous et les réaliser, partager nos passions...',
      himandher_title: 'Elle Et Lui',
      himandher_casey_1: 'Chanteuse, saxophoniste, contrebassiste, arrangeuse, ukuleliste, guitariste...',
      himandher_casey_2: 'Casey Jayne est née sur la côte Est des États-Unis, et arrivée en France à 19 ans pour étudier le saxophone classique. Elle enseigne la musique (diplômée DEM et bachelor of music - NYU) et l\'anglais (diplômée TOEFL), et offre ses talents de multi-instrumentiste à de nombreux projets musicaux, avant de commencer à monter les siens.',
      himandher_casey_3: 'Actuellement, elle chante et joue, en trio dans La Tartine, en duo avec Cyril dans La Méchante et le Connard, et dans le grand dîner-spectacle \"Buffalo Bill\'s Wild West Show\" à Disney Village.',
      himandher_cyril_1: 'Bassiste et contrebassiste, auteur-compositeur, percussioniste, MAOiste, guitariste...',
      himandher_cyril_2: 'Cyril est né et a grandi dans les Yvelines, dans une famille de musiciens, et a d\'abord exercé ses talents au piano. Il se décide à 19 ans pour la guitare basse et se jette dans toutes sortes de projets musicaux. Il est formateur pour les métiers de l\'éducation (diplômé BAFD) et enseigne la musique (diplômé DEM MAA).',
      himandher_cyril_3: 'Actuellement, il joue en trio dans La Tartine, en duo avec Casey dans La Méchante et le Connard, a composé quelques albums de musique électronique et travaille à l\'édition de son livre de poésie.',
      ourhistory_title: 'notre histoire',
      ourhistory_date_1: '2010: collègues à l\'école de musique "La Boîte à Musique"',
      ourhistory_date_2: '2012: formation du duo "La Méchante et le Connard"',
      ourhistory_date_3: '2013: formation du trio "La Tartine" avec Raphaël Naveau',
      ourhistory_date_4: '2015: mariage',
      ourhistory_date_5: '2016: installation à Bué et création du CEC'
    },
    activities: {
      music_classes: 'cours de musique',
      music_classes_blurb: 'Cours individuels d\'instruments, et/ou cours collectifs. De 0 à 105 ans.',
      english_classes: 'cours d\'anglais',
      english_classes_blurb: 'En solo ou en petits groupes, tous âges et niveaux.',
      cultural_activities: 'actions culturelles',
      cultural_activities_blurb: 'Les rendez-vous de l\'année. Partageons nos projets !',
      academic_support: 'soutien scolaire',
      academic_support_blurb: 'En solo ou en petits groupes, primaire et collège.',
      baby_music: 'bébé musique',
      little_musicians: 'petits musiciens',
      musical_awakening: 'éveil musical',
      modal: {
        english: {
          title: 'Cours D\'Anglais',
          content: 'On apprend mieux quand on se sent bien et qu\'on s\'amuse. \n\nOn apprend mieux en prenant des risques, en faisant des erreurs et en étendant ses propres limites. \n\nNos cours d\'anglais suivent cette philosophie, en créant un environnement confortable, pour se concentrer sur la conversation et la fluidité, autour des centres d\'intérêt de l\'élève. Pour enfants ou adultes, les cours s\'appuient sur la compréhension orale et écrite, l\'écriture et la prononciation. Les cours peuvent s\'orienter vers du soutien scolaire, la préparation au bac ou l\'anglais des affaires. Que vous cherchiez à apprendre l\'anglais pour le travail, pour l\'école, pour les voyages ou pour le plaisir, les cours sont personnalisés pour vous aider à exceller.\n\nLes cours collectifs pour enfants sont ludiques, et ont pour but d\'améliorer le vocabulaire, la conversation et la compréhension orale, tout en explorant la diversité des cultures anglophones.\n\nLes séances ont lieu chaque semaine,horaires à définir selon les demandes.\n\n​Tous âges et tous niveaux.\n\n​Voir aussi : les Thés à l\'Anglaise du dimanche !',
        },
        academic: {
          title: 'Soutien Scolaire',
          content: 'Tout le monde mérite de réussir.\n\nEt comme on a parfois besoin d\'un petit coup de pouce... nous sommes là pour aider !\n\nConfiance en soi, méthode, patience, révisions, approfondissement, démystification,encouragements, réexplication, ou juste un déclic... On n\'a pas tous besoin de la même chose. Alors, au CEC, on cherche ensemble (en groupe ou en face à face), et on partage les efforts pour que tous les élèves puissent trouver une chance supplémentaire de renforcer leurs points forts et de surmonter leurs difficultés.\n\nEt comme on n\'a toujours pas trouvé de raison de s\'y ennuyer, nous venons toujours avec notre réserve habituelle de bonne humeur !\n\nLes séances ont lieu chaque semaine,horaires à définir selon les demandes.\n\n​Du CP à la 3ème.(Peut-être un peu au-delàselon les matières et les besoins.)'
        }
      }
    },
    music_courses: {
      blurb: 'Nos cours sont personnalisés, parce que nous croyons que chacun est unique. \n\n Nos cours sont bons pour le moral, parce que nous cherchons toujours le juste équilibre entre effort et plaisir. \n\n Nos cours sont plus qu\'un enseignement technique, parce que nous partageons aussi notre passion. \n\n Nos cours sont d\'abord un dialogue et un échange humain, parce qu\'enseigner c\'est d\'abord écouter. \n\n Nous parlons de notes et de doigtés, d\'intentions et d\'émotions, de projets et d\'enthousiasme, de travail et de détente, parce que tout cela fait partie du quotidien du musicien. \n\n Nos cours sont pleins de bonne humeur, parce qu\'on est comme ça !',
      instruments: {
        instruments: 'instruments',
        guitar: 'guitare',
        saxophone: 'saxophone',
        bass_guitar: 'basse électrique',
        double_bass: 'contrebasse',
        ukulele: 'ukulélé',
        voice: 'chant',
        mao: 'mao',
        percussion: 'percussions',
        piano_beginners: 'piano (débutants)',
        recorder_beginners: 'flûte à bec (débutants)'
      },
      courses: {
        collectivecourses: 'cours collectifs',
        infantcourses: 'cours pour les enfants',
        zeroto18months: '0 à 18 mois: bébé musique (parents-enfants)',
        twoto3yrs: '2 à 3 ans: petits musiciens',
        threeto6yrs: 'éveil musical',
        from6yrs: 'à partir de 6 ans: formation musicale, découvertes instrumentales',
        allAges: {
          forallages: 'cours pour tous les âges',
          training: 'formation musicale',
          discovery: 'découvertes instrumentales',
          analysis: 'analyse et histoire de la musique',
          improvisation: 'improvisation',
          composition: 'composition et arrangements',
          workshop: 'atelier groupe et/ou orchestre'
        }
      },
      modal: {
        baby: {
          title: 'BÉBÉ MUSIQUE',
          content: 'Pour les bébés de 0 à 18 mois et leurs parents. Découvrez avec votre bébé le monde des sons, du rythme et de la musique. Chaque séance dure 45 minutes. Nous allons chanter des chansons ensemble (en français et en anglais !), découvrir divers instruments, et tout au long de l\'année, faire nos premières expériences musicales : jouer des instruments, écouter, chanter, danser, s\'amuser et se détendre. Tout en jouant ensemble, les enfants développent un sens naturel du rythme et du geste musical, le goût d\'une écoute attentive, et une nouvelle sensibilité aux sons qui les entourent. C\'est en plus l\'occasion d\'un beau moment de partage parents/enfants, et de beaux échanges avec les nouveaux amis !'
        },
        little: {
          title: 'PETITS MUSICIENS',
          content: 'Pour les petits de 2 à 3 ans, avec ou sans parents ! Cet atelier est spécialement adapté pour les petits qui ne sont plus des bébés, mais ne vont pas encore à l\'école.Chaque séance dure 45 minutes. Les parents sont toujours bienvenus pour participer pleinement aux activités, ou choisir, en concertation avec l\'enfant, de se retirer plus ou moins progressivement. (on ouvre avec plaisir la pièce voisine pour les parents qui souhaitent rester à côté !)Ensemble, on chante et on écoute, on bouge et on se relaxe, on observe et on manipule des instruments et autres objets sonores, on explore les possibilités de nos voix, de nos mains et de nos pieds, on prend conscience de nos émotions et sensations liées à la musique...Tout en jouant ensemble, les enfants découvrent le plaisir d\'être musiciens. Il développent un sens naturel du rythme et du geste musical, le goût d\'une écoute active, et une sensibilité accrue aux sons qui les entourent, ou qu\'ils créent !'
        },
        awake: {
          title: 'ÉVEIL MUSICAL',
          content: 'Pour les enfants de 3 à 6 ans. Le contenu est ouvertement annoncé aux enfants : à chaque séance, en 45 minutes, on veut pouvoir, d\'une façon ou d\'une autre, découvrir un instrument, écouter de la musique, s\'exprimer musicalement en solo et/ou en groupe, faire au moins un petit jeu autour de la musique, et probablement finir en lisant une histoire en rapport avec la musique. Autour de ces lignes directrices, le déroulement d\'une séance et l\'évolution au cours de l\'année dépendent de chaque groupe, des réactions des enfants, de leurs questions et de leurs réponses, de leurs idées, inspirations et coups de cœur, de leurs trouvailles et inventions... Le professeur est là pour leur donner matière à découvrir et s\'émerveiller, pour stimuler leur enthousiasme et leur curiosité, qui sont les vrais moteurs de leur apprentissage. Selon les groupes, on peut même parfois aller plus loin et, par exemple, introduire quelques notions de l\'écriture musicale, prendre le temps de fabriquer nos propres instruments, ou se lancer dans un projet de création musicale !'
        }
      }
    },
    cultural_actions: {
      title: 'actions culturelles',
      blurb: 'La première chose à dire, c\'est que toutes vos idées sont les bienvenues, par principe. On vous propose les nôtres :',
      participate: 'Nos projets futurs incluent: espace d\'exposition, studio d\'enregistrement, résidences d\'artistes, répétitions, concerts et spectacles, ciné-famille... \n\n Vous avez d\'autres idées à partager ?\n Venez nous en parler !'
    }

}
