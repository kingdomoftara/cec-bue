/* eslint-disable */
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import {fr} from './fr.js'
import {en} from './en.js'

Vue.use(VueI18n)
 
const messages = {
    fr,
    en
}

export const i18n = new VueI18n({
    locale: 'fr',
    fallbackLocale: 'fr', 
    messages
})