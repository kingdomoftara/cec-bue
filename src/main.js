import Vue from 'vue'
import Vuelidate from 'vuelidate'
import VueRouter from 'vue-router'
import App from './App.vue'

import {i18n} from './plugins/i18n/i18n'
import {routes} from './routes'

Vue.config.productionTip = false

Vue.use(VueRouter)
const router = new VueRouter({
  /** problem with Vue application in Gitlab, need to disable history mode*/
  //mode: 'history',
  routes
})

Vue.use(Vuelidate)

require('./css/style.css')

Vue.config.productionTip = false

new Vue({
  i18n,
  router,
  render: h => h(App),
}).$mount('#app')
