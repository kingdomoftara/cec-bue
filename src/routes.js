import Home from './components/Home.vue'
import AboutUs from './components/AboutUs.vue'
import Activities from './components/Activities.vue'
import Contact from './components/Contact.vue'
import CulturalActions from './components/CulturalActions.vue'
import MusicCourses from './components/MusicCourses.vue'
import Prices from './components/Prices.vue'

export const routes = [
    {
        path: '',
        name: 'home',
        component: Home
    },
    {
        path: '/nous',
        name: 'aboutus',
        component: AboutUs

    },
    {
        path: '/activities',
        name: 'activities',
        component: Activities
    },
    {
        path: '/musiccourses',
        name: 'musiccourses',
        component: MusicCourses
    },
    {
        path: '/culture',
        name: 'culture',
        component: CulturalActions

    },
    {
        path: '/prices',
        name: 'prices',
        component: Prices
    },
    {
        path: '/contact',
        name: 'contact',
        component: Contact

    },
    {
        path: '*',
        redirect: '/'
    }

]