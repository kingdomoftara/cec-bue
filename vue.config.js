 module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
  ? '/cec-bue/'
  : '/',
   productionSourceMap: false
}
